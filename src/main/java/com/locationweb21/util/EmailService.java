package com.locationweb21.util;

public interface EmailService {
	 public void sendSimpleMessage(String to, String subject, String text) ;

}
