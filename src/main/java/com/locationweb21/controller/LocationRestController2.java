package com.locationweb21.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.locationweb21.entities.Locations;
import com.locationweb21.repositories.LocationsRepository;

@RestController
@RequestMapping("/Location")
public class LocationRestController2 {
	@Autowired
	private LocationsRepository locationsRepo;
	
	@GetMapping
	public List<Locations> getAllLocation(){
		List<Locations> location = locationsRepo.findAll();
		return location;
	}
	@PostMapping
	public void saveLocation(@RequestBody Locations location) {
		locationsRepo.save(location);
	}
	@PutMapping
	public void updateLocation(@RequestBody Locations location) {
		locationsRepo.save(location);
	}
	@DeleteMapping("/delete/{id}")
	public void deleteLocation(@PathVariable long id) {
		locationsRepo.deleteById(id);
	}

}
