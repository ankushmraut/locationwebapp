package com.locationweb21.controller;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

import com.locationweb21.entities.Locations;
import com.locationweb21.repositories.LocationsRepository;

@RestController
public class LocationRestController {
	@Autowired
	LocationsRepository locationRepo;
	
	@RequestMapping("location/{id}")
	
	public Locations getLocationById(@PathVariable("id") long id) {
		if(id==0) {
			throw new LocationNotFound();
		}
		Optional<Locations> locationById = locationRepo.findById(id);
		Locations location = locationById.get();
		return location;
	}

}
