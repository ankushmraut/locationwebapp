package com.locationweb21.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.locationweb21.dto.UpdateData;
import com.locationweb21.entities.Locations;
import com.locationweb21.services.LocationService;
import com.locationweb21.util.EmailService;

@Controller
public class LocationsController {
	@Autowired
	private LocationService locationservice;
	@Autowired
	private EmailService emailService;
	
	@RequestMapping("/showLocation")
	public String showLocationPage() {
		 
		return "saveLocation";
	}
	@RequestMapping("/saveLocationData")
	public String saveLocationInfo(@ModelAttribute("location") Locations location,ModelMap modelmap) {
		
		locationservice.saveLocation(location);
		
		modelmap.addAttribute("msg", "Record is save successfully!");
	//	emailService.sendSimpleMessage("ankushraut1281997@gmail.com", "Test", "Location is save successfully!");
		return "saveLocation";
		
	}
	@RequestMapping("/getLocations")
	public String getLocations(ModelMap modelmap) {
		List<Locations> locations = locationservice.getLocationsDetails();
		modelmap.addAttribute("locations", locations);
		return"searchResults";
	}
	@RequestMapping("/deleteLocation")
	public String delete(@RequestParam("id") long id, ModelMap modelmap) {
		locationservice.deleteLocationById(id);
		List<Locations> locations = locationservice.getLocationsDetails();
		modelmap.addAttribute("locations", locations);
		return"searchResults";
	}
	@RequestMapping("/update")
	public String updateLocation(@RequestParam("id") long id, ModelMap modelmap) {
		
		Locations location = locationservice.searchById(id);
		modelmap.addAttribute("location", location);

		return"updateLocation";
	}
	@RequestMapping("/updateLocationData")
	public String updateLocationData(UpdateData updateData, ModelMap modelmap) {
		long  id = updateData.getId();
		String codes = updateData.getCodes();
		String name = updateData.getName();
		String type = updateData.getType();
		Locations location =new Locations();
		location.setId(id);
		location.setName(name);
		location.setCodes(codes);
		location.setType(type);
	locationservice.saveLocation(location);
	List<Locations> locations = locationservice.getLocationsDetails();
	modelmap.addAttribute("locations", locations);
	return"searchResults";
	}

	
	
	//	@RequestMapping("/saveLocationData")
//	public String saveLocationInfo(@RequestParam("id") long id,@RequestParam("codes") String codes,@RequestParam("name")String name,@RequestParam("type") String type,ModelMap modelmap ) {
//		Locations location =new Locations();
//		location.setId(id);
//		location.setName(name);
//		location.setCodes(codes);
//		location.setType(type);
//		
//		locationservice.saveLocation(location);
//		modelmap.addAttribute("msg", "Record is save successfully!");
//		return "saveLocation";
//		
//	}
	
//	@RequestMapping("/saveLocationData")
//	public String saveLocationInfo(LocationData locationData ,ModelMap modelmap ) {
//		Locations location =new Locations();
//		location.setId(locationData.getId());
//		location.setName(locationData.getName());
//		location.setCodes(locationData.getCodes());
//		location.setType(locationData.getType());
//		
//		locationservice.saveLocation(location);
//		modelmap.addAttribute("msg", "Record is save successfully!");
//		return "saveLocation";
//		
//	}

}
