package com.locationweb21.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.locationweb21.entities.Locations;

public interface LocationsRepository extends JpaRepository<Locations, Long> {

}
