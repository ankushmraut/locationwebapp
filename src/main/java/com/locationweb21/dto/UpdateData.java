package com.locationweb21.dto;

public class UpdateData {
	private int id;
	private String Codes;
	private String name;
	private String type;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getCodes() {
		return Codes;
	}
	public void setCodes(String codes) {
		Codes = codes;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}

}
