package com.locationweb21.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.locationweb21.entities.Locations;
import com.locationweb21.repositories.LocationsRepository;

@Service
public class LocationServiceImpl implements LocationService {
	@Autowired
	private LocationsRepository locationRepo;

	@Override
	public void saveLocation(Locations location) {
		locationRepo.save(location);
	}

	@Override
	public List<Locations> getLocationsDetails() {
		List<Locations> locations = locationRepo.findAll();
		return locations;
	}

	@Override
	public void deleteLocationById(long id) {
		  locationRepo.deleteById(id);
	}

	@Override
	public Locations searchById(long id) {
		  Optional<Locations> locationById= locationRepo.findById(id);
		  Locations location = locationById.get();
   return location;
	}

}
