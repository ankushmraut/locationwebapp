package com.locationweb21.services;

import java.util.List;

import com.locationweb21.entities.Locations;

public interface LocationService {
public void saveLocation(Locations location);
public List<Locations> getLocationsDetails();
public void deleteLocationById(long id);
public Locations searchById(long id);
}
