package com.locationweb21;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Locationweb21Application {

	public static void main(String[] args) {
		SpringApplication.run(Locationweb21Application.class, args);
	}

}
